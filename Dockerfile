FROM node:13.8.0

RUN npm i -g pnpm \
    && npm config set verify-store-integrity false
